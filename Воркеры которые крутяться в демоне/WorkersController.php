<?php

namespace app\modules\track\commands;

use app\modules\track\services\EventsNew;
use app\modules\track\services\OlapImport\EventsImportBigQuery;
use app\modules\track\services\OlapImport\EventsImportClickHouse;
use app\modules\track\services\PaymentProcessing;
use app\modules\track\extensions\controllers\CliController;

class WorkersController extends CliController
{
    protected $timeStart         = 0;
    protected $periodExitProcess = 3600; // seconds

    protected $eventsCountIteration = 500;

    protected $returnNewEvents = false;

    /**
     * Generate payment transactions by conversions/clicks/impressions
     */
    public function actionPayments() : void
    {
        $processor = new PaymentProcessing();

        $result = $processor->processData($this->eventsCountIteration);

        $this->printErrors($processor->getErrors());
        unset($processor);

        $message = 'Payment Processing Stats: impressions - ' . $result['impressions']
            . ', clicks - ' . $result['clicks'] . ', conversions - ' . $result['conversions'];
        $this->echo($message, true);
    }

    /**
     * Insert new events into Google Big Query table
     */
    public function actionEvent2BigQuery() : void
    {
        $processor = new EventsImportBigQuery();

        if (!$processor->isConfigurationOk()) {
            $this->echo("ERROR! BigQuery configuration");
            return;
        }

        $result = $processor->processData();

        $this->printErrors($processor->getErrors());
        unset($processor);

        $message = 'Event2BigQuery Stats: ' . $result;
        $this->echo($message, true);
    }

    /**
     * Insert new events into Click House table
     */
    public function actionEvent2ClickHouse() : void
    {
        $processor = new EventsImportClickHouse();

        $result = $processor->processData(true);

        $this->printErrors($processor->getErrors());
        unset($processor);

        $message = 'Event2ClickHouse Stats: ' . $result;
        $this->echo($message, true);
    }

    /**
     * Process new tracking events - summarize stats, detects browser/platform/country/state/city
     */
    public function actionEventsNew() : ?int
    {
        $processor = new EventsNew();
        $result    = 0;

        try {
            $result = $processor->processData();

            $this->printErrors($processor->getErrors());
            unset($processor);

            $message = 'EventsNew Stats: ' . $result;
            $this->echo($message, true);
        } catch (\Throwable $e) {
            $this->printErrors([$e]);
        }

        return $this->returnNewEvents? $result : null;
    }

    /**
     * Run Lynx processing daemon
     */
    public function actionRun()
    {
        $eventsTotal = 0;
        $this->timeStart = microtime(true);

        $this->returnNewEvents = true;

        while (true) {
            $counter = $this->actionEventsNew();
            $this->actionPayments();
            $this->actionEvent2BigQuery();
            $this->actionEvent2ClickHouse();

            if ($counter > 0) {
                $eventsTotal += $counter;
            } else {
                sleep(1);
            }

            $uptime = microtime(true) - $this->timeStart;
            $this->echo("Uptime: " . round($uptime, 1). "s");

            if ($uptime > $this->periodExitProcess) {
                $this->echo("Exit: max uptime reached");
                break;
            }

            if ($this->isShouldTerminate()) {
                $this->echo("Exit: signal received");
                break;
            }
        }

        $this->echo("DONE ({$eventsTotal} events)");
    }

    /**
     * Print errors to console
     * @param \Throwable[] $errorsList
     */
    protected function printErrors(array $errorsList) : void
    {
        foreach ($errorsList as $error) {
            /** @var \Throwable $error */
            $this->echo("Error: " . $error->getMessage() . " (File: {$error->getFile()}:{$error->getLine()})\n{$error->getTraceAsString()}", true);
        }
    }
}
