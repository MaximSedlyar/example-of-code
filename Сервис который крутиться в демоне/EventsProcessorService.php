<?php

declare(strict_types=1);

namespace spixel\services\DaemonsLogic\EventsProcessor;

use spixel\models\Session;
use spixel\models\SessionPage;
use spixel\models\SessionPageTag;
use spixel\models\SessionPageVariable;
use spixel\models\SiteTag;
use spixel\services\DaemonsLogic\AbstractProcessorService;
use spixel\services\DaemonsLogic\DaemonInterface;
use spixel\services\DaemonsLogic\DaemonServiceTrait;
use spixel\services\DaemonsLogic\EventsDaemonInterface;
use spixel\services\DaemonsLogic\ProcessorServiceTrait;
use yii\base\InvalidConfigException;
use yii\db\DataReader;
use yii\db\Exception;
use const YII_ENV_DEV;

/**
 * Class EventsProcessorService
 *
 * @package spixel\services\DaemonsLogic\EventsProcessor
 */
class EventsProcessorService implements DaemonInterface, EventsDaemonInterface, EventsProcessorInterface
{
    use ProcessorServiceTrait;
    use DaemonServiceTrait;

    public const LOG_MSG_PREFIX = '[Events Processor] ';

    public const DAEMON_NAME = 'EventsProcessor';

    public function run(): bool
    {
        $this->isAcquireDaemonLock();

        $sessionPage = $this->getSessionPageData();

        if (empty($sessionPage)) {
            $this->isReleaseDaemonLock();

            return false;
        }

        $sessionPageId = (int) $sessionPage['id'];
        $sessionId = (int) $sessionPage['sessionId'];
        $pageId = (int) $sessionPage['pageId'];

        $session = $this->getSessionData($sessionId);

        $siteId = (int) $session['siteId'];
        $visitorId = (int) $session['visitorId'];

        $this->outputLogMsg("set session page ({$sessionPageId}) status to '3' (process)");
        $this->setSessionPageStatusToProcess($sessionPageId);

        $this->isReleaseDaemonLock();

        $this->outputLogMsg('get session page data from Redis');
        $pageData = $this->sessionPageEventsService->getAllData($sessionPageId);

        if (empty($pageData)) {
            $this->outputLogMsg('no session page data were found in Redis, 
            set session status to "6" (error) and session page status to "5" (error)');

            $this->setSessionStatusToError($sessionId);
            $this->setSessionPageStatusToError($sessionPageId);

            return false;
        }

        $this->outputLogMsg('processing data');
        $this->eventsProcessorHandler
            ->setSessionPageId($sessionPageId)
            ->setPageWidth((int)$sessionPage['pageWidth'])
            ->loadData($pageData)
            ->processing();

        $this->formAnalyticSaverService->save(
            $pageId,
            $sessionId,
            $this->eventsProcessorHandler->getResult()[AbstractProcessorService::RESULT_FORMS_LIST_ON_PAGE],
            $this->eventsProcessorHandler->getResult()[AbstractProcessorService::RESULT_FORMS_ANALYTICS]
        );

        $this->outputLogMsg('set processed data to Redis');
        $this->predisCacheService->getClient()->hset(
            self::PAGE_STATS_PREFIX_KEY . $pageId,
            $sessionPageId,
            $this->eventsProcessorHandler->getResultString()
        );
        $this->addSystemTags(
            $siteId,
            $sessionId,
            $sessionPageId,
            $this->eventsProcessorHandler->getResult()[AbstractProcessorService::RESULT_TAGS]
        );

        $this->addJsApi(
            $siteId,
            $sessionId,
            $sessionPageId,
            $visitorId,
            $this->eventsProcessorHandler->getResult()[AbstractProcessorService::RESULT_JS_API]
        );

        $this->isAcquirePageIdLock($pageId);

        $this->outputLogMsg('add session page key to Redis for further usage');
        $pagesProcessTime = $this->getPageProcessTime();
        $this->predisCacheService->getClient()->zadd(
            self::PAGE_EVENTS_PROCESSED_KEY,
            'NX',
            $pagesProcessTime,
            $pageId
        );

        $this->isReleasePageIdLock($pageId);

        $this->outputLogMsg('updating session processed pages count by one');
        $this->updateSessionProcessedPages($sessionId);

        return true;
    }

    /**
     * @return int
     */
    private function getPageProcessTime(): int
    {
        return time() + (YII_ENV_DEV ? 1 : 15);
    }

    /**
     * @param int   $siteId
     * @param int   $sessionId
     * @param int   $sessionPageId
     * @param array $tags
     */
    private function addSystemTags(int $siteId, int $sessionId, int $sessionPageId, array $tags): void
    {
        $friction = 0;
        $totalCountMouseOut = SessionPageTag::getTotalCountSessionMouseOutTags($siteId, $sessionId);

        foreach ($tags as $tagName => $params) {
            if ($tagName == SiteTag::TAG_MOUSE_OUT) {
                $score = (0 == $totalCountMouseOut) ? 1 : 0;
            } else {
                $score = $params[EventsProcessorHandler::TAGS_FRICTION_SCORE];
            }

            $newTag = SessionPageTag::addTagSystem(
                $siteId,
                $sessionPageId,
                $tagName,
                $params[EventsProcessorHandler::TAGS_DETAIL],
                $score
            );

            if (null !== $newTag) {
                $friction += (int) $newTag->friction;
            }
        }

        if ($friction > 0) {
            $this->updateSessionFrictions($sessionId, $friction);
            $this->setSessionPageFriction($sessionPageId, $friction);
        }
    }

    /**
     * @param int   $siteId
     * @param int   $sessionId
     * @param int   $sessionPageId
     * @param int   $visitorId
     * @param array $jsApiData
     *
     * @throws Exception
     */
    private function addJsApi(int $siteId, int $sessionId, int $sessionPageId, int $visitorId, array $jsApiData): void
    {
        $this->addJsApiTags($siteId, $sessionPageId, $jsApiData[EventsProcessorHandler::EVENT_JS_API_TAG]);
        $this->addJsApiVariables($siteId, $sessionPageId, $jsApiData[EventsProcessorHandler::EVENT_JS_API_VARIABLE]);
        $this->updateJsApiUser($visitorId, $jsApiData[EventsProcessorHandler::EVENT_JS_API_USER]);
        $this->updateJsApiFavorite($sessionId, $jsApiData[EventsProcessorHandler::EVENT_JS_API_FAVORITE]);
    }

    /**
     * @param int   $siteId
     * @param int   $sessionPageId
     * @param array $tags
     */
    private function addJsApiTags(int $siteId, int $sessionPageId, array $tags): void
    {
        foreach ($tags as $tag) {
            SessionPageTag::addTagUserByName($siteId, $sessionPageId, $tag);
        }
    }

    /**
     * @param int   $siteId
     * @param int   $sessionPageId
     * @param array $variables
     */
    private function addJsApiVariables(int $siteId, int $sessionPageId, array $variables): void
    {
        foreach ($variables as $key => $value) {
            SessionPageVariable::addVariableByKeyAndName($siteId, $sessionPageId, $key, $value);
        }
    }

    /**
     * @param int   $visitorId
     * @param array $user
     *
     * @throws Exception
     */
    private function updateJsApiUser(int $visitorId, array $user): void
    {
        if (!empty($user) && isset($user[0])) {
            $name = $user[0];

            $sql = sprintf('
            UPDATE spixel_visitor AS sv 
            SET sv.name = \'%s\' 
            WHERE sv.id = %d',
                $name,
                $visitorId
            );
            $this->db->createCommand($sql)->execute();
        }
    }

    /**
     * @param int   $sessionId
     * @param array $favorite
     *
     * @throws Exception
     */
    private function updateJsApiFavorite(int $sessionId, array $favorite): void
    {
        if (!empty($favorite) && isset($favorite[0])) {
            $isFavorite = $favorite[0];

            $sql = sprintf('
            UPDATE spixel_session AS sp 
            SET sp.isFavorite = %s 
            WHERE sp.id = %d',
                $isFavorite,
                $sessionId
            );
            $this->db->createCommand($sql)->execute();
        }
    }

    /**
     * @param int $sessionId
     * @throws Exception
     * @throws InvalidConfigException
     */
    private function updateSessionProcessedPages(int $sessionId): void
    {
        $sql = sprintf(
            'UPDATE %s SET pagesProcessed = pagesProcessed + 1 WHERE id = %d',
            Session::tableName(),
            $sessionId
        );
        $this->db->createCommand($sql)->execute();
    }

    /**
     * @return array|false|DataReader
     * @throws Exception
     * @throws InvalidConfigException
     */
    private function getSessionPageData()
    {
        $sql = sprintf(
            'SELECT id, sessionId, pageId, pageWidth FROM %s WHERE statusId = %d LIMIT 1;',
            SessionPage::tableName(),
            SessionPage::STATUS_LEAVE
        );

        return $this->db->createCommand($sql)->queryOne();
    }

    /**
     * @param int $sessionPageId
     *
     * @return array|false|DataReader
     * @throws Exception
     * @throws InvalidConfigException
     */
    private function getSessionData(int $sessionPageId)
    {
        $sql = sprintf(
            'SELECT id, siteId, visitorId FROM %s WHERE id = %d LIMIT 1;',
            Session::tableName(),
            $sessionPageId
        );

        return $this->db->createCommand($sql)->queryOne();
    }

    /**
     * @param int $sessionPageId
     * @throws Exception
     * @throws InvalidConfigException
     */
    private function setSessionPageStatusToProcess(int $sessionPageId): void
    {
        $sql = sprintf(
            'UPDATE %s SET statusId = %d WHERE id = %d;',
            SessionPage::tableName(),
            SessionPage::STATUS_EVENTS_PROCESS,
            $sessionPageId
        );

        $this->db->createCommand($sql)->execute();
    }

    /**
     * @param int $sessionId
     * @throws Exception
     * @throws InvalidConfigException
     */
    private function setSessionStatusToError(int $sessionId): void
    {
        $sql = sprintf(
            'UPDATE %s SET statusId = %d WHERE id = %d;',
            Session::tableName(),
            Session::STATUS_SAVE_ERROR,
            $sessionId
        );

        $this->db->createCommand($sql)->execute();
    }

    /**
     * @param int $sessionPageId
     * @throws Exception
     * @throws InvalidConfigException
     */
    private function setSessionPageStatusToError(int $sessionPageId): void
    {
        $sql = sprintf(
            'UPDATE %s SET statusId = %d WHERE id = %d;',
            SessionPage::tableName(),
            SessionPage::STATUS_EVENTS_ERROR,
            $sessionPageId
        );

        $this->db->createCommand($sql)->execute();
    }

    /**
     * @param int $pageId
     * @return bool
     */
    private function isAcquirePageIdLock(int $pageId): bool
    {
        return $this->mutexService->isReleaseLock((string) $pageId);
    }

    /**
     * @param int $pageId
     * @return bool
     */
    private function isReleasePageIdLock(int $pageId): bool
    {
        return $this->mutexService->isReleaseLock((string) $pageId);
    }

    /**
     * @return bool
     */
    private function isAcquireDaemonLock(): bool
    {
        return $this->mutexService->isAcquireLock(self::DAEMON_NAME);
    }

    /**
     * @return bool
     */
    private function isReleaseDaemonLock(): bool
    {
        return $this->mutexService->isReleaseLock(self::DAEMON_NAME);
    }
}
