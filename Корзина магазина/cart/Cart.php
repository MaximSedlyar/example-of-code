<?php

namespace frontend\modules\cart;

use frontend\modules\cart\cost\CalculatorInterface;
use frontend\modules\cart\storage\StorageInterface;

/**
 * Class Cart
 *
 * @package frontend\modules\cart
 */
class Cart
{
    private StorageInterface $storage;
    private CalculatorInterface $calculator;

    /** @var CartItem[] */
    private $items;

    /**
     * Cart constructor.
     * @param StorageInterface $storage
     * @param CalculatorInterface $calculator
     */
    public function __construct(StorageInterface $storage, CalculatorInterface $calculator)
    {
        $this->storage = $storage;
        $this->calculator = $calculator;
    }

    /**
     * @{inheritDoc}
     */
    private function loadItems(): void
    {
        if ($this->items === null) {
            $this->items = $this->storage->load();
        }
    }

    /**
     * @{inheritDoc}
     */
    private function saveItems(): void
    {
        $this->storage->save($this->items);
    }

    /**
     * @return CartItem[]
     */
    public function getItems(): array
    {
        $this->loadItems();
        return $this->items;
    }

    /**
     * @{inheritDoc}
     */
    public function clear(): void
    {
        $this->items = [];
        $this->saveItems();
    }

    /**
     * @param string $uuid
     * @param int    $variationId
     */
    public function remove(string $uuid, int $variationId): void
    {
        $this->loadItems();
        $id = CartItem::generateId($uuid, $variationId);
        if (array_key_exists($id, $this->items)) {
            unset($this->items[$id]);
        }
        $this->saveItems();
    }

    /**
     * @param string $uuid
     * @param int    $variationId
     * @param int    $count
     * @param float  $price
     * @param int    $maxQuantity
     * @param bool   $change
     *
     * @throws MoreThanAvailableException
     */
    private function addItem(
        string $uuid,
        int $variationId,
        int $count,
        float $price,
        int $maxQuantity,
        bool $change = false
    ): void {
        $this->loadItems();
        $id = CartItem::generateId($uuid, $variationId);
        $current = isset($this->items[$id]) ? $this->items[$id]->getCount() : 0;
        if ($change) {
            $current = 0;
        }
        if ($current + $count > $maxQuantity) {
            throw new MoreThanAvailableException(
                \Yii::t('order', 'max_quantity_exception', ['quantity' => $maxQuantity])
            );
        }
        $this->items[$id] = new CartItem($id, $uuid, $variationId, $current + $count, $price);
        $this->saveItems();
    }

    /**
     * @param $uuid
     * @param $variationId
     * @param $count
     * @param $price
     * @param $maxQuantity
     * @throws MoreThanAvailableException
     */
    public function add(string $uuid, int $variationId, int $count, float $price, int $maxQuantity)
    {
        $this->addItem($uuid, $variationId, $count, $price, $maxQuantity);
    }

    /**
     * @param string $uuid
     * @param int    $variationId
     * @param int    $count
     * @param float  $price
     * @param int    $maxQuantity
     *
     * @throws MoreThanAvailableException
     */
    public function change(string $uuid, int $variationId, int $count, float $price, int $maxQuantity): void
    {
        $this->addItem($uuid, $variationId, $count, $price, $maxQuantity, true);
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        $this->loadItems();
        return $this->calculator->getCost($this->items);
    }
}
