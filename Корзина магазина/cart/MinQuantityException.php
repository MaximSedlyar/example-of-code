<?php


namespace frontend\modules\cart;

/**
 * Class MinQuantityException
 *
 * @package frontend\modules\cart
 */
class MinQuantityException extends \Exception
{
}
