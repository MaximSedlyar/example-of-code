<?php


namespace frontend\modules\cart;

/**
 * Class MoreThanAvailableException
 *
 * @package frontend\modules\cart
 */
class MoreThanAvailableException extends \Exception
{
}
