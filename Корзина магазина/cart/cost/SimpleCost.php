<?php

namespace frontend\modules\cart\cost;

use frontend\modules\cart\CartItem;

/**
 * Class SimpleCost
 *
 * @package frontend\modules\cart\cost
 */
class SimpleCost implements CalculatorInterface
{
    /**
     * @param CartItem[] $items
     * @return float|int
     */
    public function getCost(array $items): float
    {
        $cost = 0;
        foreach ($items as $item) {
            $cost += $item->getCost();
        }
        return $cost;
    }
}
