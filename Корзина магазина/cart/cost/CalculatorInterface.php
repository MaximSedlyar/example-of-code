<?php

namespace frontend\modules\cart\cost;

use frontend\modules\cart\CartItem;

/**
 * Interface CalculatorInterface
 *
 * @package frontend\modules\cart\cost
 */
interface CalculatorInterface
{
    /**
     * @param CartItem[] $items
     * @return float
     */
    public function getCost(array $items): float;
}
