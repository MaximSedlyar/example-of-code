<?php

namespace frontend\modules\cart;

use api\models\Product;

/**
 * Class CartItem
 *
 * @package frontend\modules\cart
 */
class CartItem
{
    /** @var int  */
    private int $id;

    /** @var string  */
    private string $uuid;

    /** @var int  */
    private int $variationId;

    /** @var int  */
    private int $count;

    /** @var float  */
    private float $price;

    /** @var Product|null  */
    private Product $product;

    /**
     * CartItem constructor.
     *
     * @param int          $id
     * @param string       $uuid
     * @param int          $variationId
     * @param int          $count
     * @param float        $price
     * @param Product|null $product
     */
    public function __construct(
        int $id,
        string $uuid,
        int $variationId,
        int $count,
        float
        $price,
        ?Product $product = null
    ) {
        $this->id = $id;
        $this->uuid = $uuid;
        $this->variationId = $variationId;
        $this->count = $count;
        $this->price = $price;
        $this->product = $product;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return int
     */
    public function getVariationId(): int
    {
        return $this->variationId;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        return $this->price * $this->count;
    }

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        if (null == $this->product) {
            $this->product = Product::find()->where(['uuid' => $this->uuid])->limit(1)->one();
        }

        return $this->product;
    }

    /**
     * @param string $uuid
     * @param int    $variationId
     *
     * @return string
     */
    public static function generateId(string $uuid, int $variationId): string
    {
        return md5($uuid . $variationId);
    }
}
