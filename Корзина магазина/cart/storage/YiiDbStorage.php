<?php

namespace frontend\modules\cart\storage;

use frontend\models\Cart;
use frontend\modules\cart\CartItem;
use yii\db\Query;

/**
 * Class YiiDbStorage
 *
 * @package frontend\modules\cart\storage
 */
class YiiDbStorage implements StorageInterface
{
    /** @var int  */
    private int $userId;

    /**
     * YiiDbStorage constructor.
     *
     * @param int $userId
     */
    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return array
     */
    public function load(): array
    {
        $rows = (new Query())
            ->select('*')
            ->from(Cart::tableName())
            ->where(['user_id' => $this->userId])
            ->all();

        $result = [];

        if ($rows) {
            foreach ($rows as $row) {
                $id = CartItem::generateId($row['uuid'], $row['variant_id']);

                $result[$id] = new CartItem($id, $row['uuid'], $row['variant_id'], $row['count'], $row['price']);
            }
        }

        return $result;
    }

    /**
     * @param array $items
     *
     * @throws \yii\db\Exception
     */
    public function save(array $items)
    {
        \Yii::$app->db->createCommand()->delete(
            Cart::tableName(),
            [
                'user_id' => $this->userId,
            ]
        )->execute();

        \Yii::$app->db->createCommand()->batchInsert(
            Cart::tableName(),
            [
                'user_id',
                'uuid',
                'variant_id',
                'count',
                'price'
            ],
            array_map(
                function (CartItem $item) {
                    return [
                        'user_id'    => $this->userId,
                        'uuid'       => $item->getUuid(),
                        'variant_id' => $item->getVariationId(),
                        'count'      => $item->getCount(),
                        'price'      => $item->getPrice()
                    ];
                },
                $items
            )
        )->execute();
    }
}
