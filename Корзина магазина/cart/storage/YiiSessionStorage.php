<?php

namespace frontend\modules\cart\storage;

use Yii;

/**
 * Class YiiSessionStorage
 *
 * @package frontend\modules\cart\storage
 */
class YiiSessionStorage implements StorageInterface
{
    /** @var mixed */
    private $key;

    /**
     * YiiSessionStorage constructor.
     *
     * @param $key
     */
    public function __construct($key)
    {
        $this->key = $key;
    }

    /**
     * @return \frontend\modules\cart\CartItem[]|mixed
     */
    public function load()
    {
        return Yii::$app->session->get($this->key, []);
    }

    /**
     * @param array $items
     */
    public function save(array $items): void
    {
        Yii::$app->session->set($this->key, $items);
    }
}
