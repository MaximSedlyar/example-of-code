<?php

namespace frontend\modules\cart\storage;

use frontend\modules\cart\CartItem;

/**
 * Interface StorageInterface
 *
 * @package frontend\modules\cart\storage
 */
interface StorageInterface
{
    /**
     * @return CartItem[]
     */
    public function load();

    /**
     * @param CartItem[] $items
     */
    public function save(array $items): void;
}
