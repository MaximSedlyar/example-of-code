<?php

namespace spixel\services\Api;

use spixel\models\Session;
use spixel\models\SessionPage;
use spixel\models\SitePage;
use spixel\models\Visitor;
use spixel\services\Crypt\HashIds\HashIdsInterface;
use spixel\services\DataFilters\AbstractFilter;
use spixel\services\DataFilters\HeatmapsFilter;
use spixel\services\DataFilters\HeatmapsFilterRules;
use spixel\services\DataSorting\SortingRulesTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\Expression;
use spixel\components\db\Query;
use yii\di\NotInstantiableException;

/**
 * Class CommonDataService
 *
 * @package spixel\services\Api
 */
class CommonDataService implements CommonDataInterface
{
    use SortingRulesTrait;

    /**
     * @var HeatmapsFilter
     */
    private ?AbstractFilter $heatmapsFilter;

    /**
     * CommonDataService constructor.
     *
     * @param HashIdsInterface $hashIdsService
     */
    public function __construct(?AbstractFilter $heatmapsFilter = null)
    {
        $this->heatmapsFilter = $heatmapsFilter;
    }

    /**
     * @inheritDoc
     */
    public function getPagesData(?int $limit = null, ?int $offset = null, ?int $pageId = null): array
    {
        $baseQuery = $this->getBaseQuery($limit, $offset);
        $this->addBaseQuerySelectClause($baseQuery);

        if (!empty($pageId)) {
            $baseQuery->andWhere('sp.pageId = :pageId', ['pageId' => $pageId]);
        }
        $baseQuery->groupBy(['sp.pageId']);
        $baseQuery = $this->applySortingRules($baseQuery);

        $query = (new Query())
            ->from(['t' => $baseQuery])
            ->innerJoin(['stp' => SitePage::tableName()], 'stp.id = t.pageId')
        ;
        $this->addGetDataQuerySelectClause($query);
        $this->applySortingRules($query);

        return $query->all();
    }

    /**
     * @inheritDoc
     */
    public function getPageData(int $pageId): array
    {
        $templateData = [
            'shortPage'    => 'Not Data',
            'page'         => 'Not Data',
            'clicks'       => 0,
            'visitTime'    => 0,
            'users'        => 0,
            'scroll'       => 0,
            'size'         => 0,
            'height'       => 0,
            'isFavorite'   => 0,
            'id'           => 0,
            'deviceTypeId' => 'Not Data',
            'title'        => 'Not Data',
            'friction'     => 0,
            'views'        => 0,
        ];

        $data = $this->getPagesData(1, 0, $pageId);

        return $data[0] ?? $templateData;
    }

    /**
     * @inheritDoc
     */
    public function getPageVisitors(int $sessionId): array
    {
        return (new Query())
            ->select([
                'sp.id AS pageId',
                'sp.urlParams AS urlParams',
                'stp.siteId AS siteId',
                'stp.url AS url',
                's.visitorId AS visitorId',
                'sv.name AS visitorName',
            ])
            ->from(['sp' => SessionPage::tableName()])
            ->leftJoin(['stp' => SitePage::tableName()], 'stp.id = sp.pageId')
            ->leftJoin(['s' => Session::tableName()], 's.id = sp.sessionId')
            ->leftJoin(['sv' => Visitor::tableName()], 'sv.id = s.visitorId')
            ->where('sp.sessionId = :sessionId', ['sessionId' => $sessionId])
            ->orderBy(['sp.id' => SORT_ASC])
            ->all();
    }

    /**
     * @inheritDoc
     */
    public function togglePageFavoriteState(int $id): void
    {
        Yii::$app->dbSpixel->createCommand()->update(
            SitePage::tableName(),
            [
                'isFavorite' => new Expression('IF(isFavorite = 1, 0, 1)'),
            ],
            'id = :id',
            ['id' => $id]
        )->execute();
    }

    /**
     * @return int
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function getPagesCount(): int
    {
        return (int) $this->getBaseQuery()->select(['COUNT(DISTINCT sp.pageId)'])->scalar();
    }

    /**
     * @inheritDoc
     */
    public function setFilter(HeatmapsFilter $filter): void
    {
        $this->heatmapsFilter = $filter;
    }

    /**
     * @inheritDoc
     */
    public function getFilter(): HeatmapsFilter
    {
        return $this->heatmapsFilter;
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     * @return Query
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    private function getBaseQuery(?int $limit = null, ?int $offset = null): Query
    {
        $query = (new Query())
            ->from(['bs' => Session::tableName()])
            ->innerJoin(['sp' => SessionPage::tableName()], 'bs.id = sp.sessionId')
            ->where('bs.statusId <> :statusId', ['statusId' => Session::STATUS_DELETED])
            ->andWhere('bs.pageEntryId > :pageEntryId', ['pageEntryId' => 0])
        ;

        $query = $this->applyFilterRules($query);
        $this->addBaseQueryLimitClause($query, $limit, $offset);

        return $query;
    }

    /**
     * @param Query $query
     *
     * @return Query
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    private function applyFilterRules(Query $query): Query
    {
        if (empty($this->heatmapsFilter)) {
            return $query;
        }

        $heatmapsFilterRules = Yii::$container->get(
            HeatmapsFilterRules::class,
            [
                $this->heatmapsFilter,
                $query,
            ]
        );

        return $heatmapsFilterRules->applyFilterRules();
    }

    /**
     * @param Query $query
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return void
     */
    private function addBaseQueryLimitClause(Query $query, ?int $limit = null, ?int $offset = null): void
    {
        $query->limit($limit)->offset($offset);
    }

    /**
     * @param Query $query
     */
    private function addBaseQuerySelectClause(Query $query): void
    {
        $query->select([
            'sp.pageId',
            'COUNT(sp.id) AS views',
            'AVG(UNIX_TIMESTAMP(sp.finishedAt) - UNIX_TIMESTAMP(sp.createdAt)) AS visitTime',
            'SUM(sp.clicks) AS clicks',
            'COUNT(DISTINCT visitorId) AS users',
            'AVG((sp.scrollHeight / sp.pageHeight) * 100) AS scroll',
            'ROUND((SUM(sp.friction) / COUNT(sp.id)), 1) AS friction',
            'bs.deviceTypeId AS deviceTypeId',
        ]);
    }

    /**
     * @param Query $query
     */
    private function addGetDataQuerySelectClause(Query $query): void
    {
        $query->select([
            'stp.id AS id',
            'stp.url AS page',
            'stp.height AS height',
            'stp.size AS size',
            'stp.title AS title',
            'stp.isFavorite AS isFavorite',
            't.views',
            't.visitTime',
            't.clicks',
            't.users',
            't.scroll',
            't.friction',
            't.deviceTypeId',
        ]);
    }
}
