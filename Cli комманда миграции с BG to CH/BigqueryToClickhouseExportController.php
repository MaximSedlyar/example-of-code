<?php

namespace app\modules\track\commands;

use app\extensions\components\storage\RedisStorage;
use app\extensions\db\BigQuery;
use app\extensions\db\clickhouse\ClickHouseConnection;
use app\modules\track\extensions\controllers\CliController;
use ClickHouseDB\Statement;
use DateTime;
use Google\Cloud\BigQuery\Date;
use Yii;

class BigqueryToClickhouseExportController extends CliController
{
    private const REDIS_START_DATE        = 'startDate';
    private const REDIS_LAST_IMPORT_DATE  = 'lastImportDate';
    private const REDIS_LAST_IMPORT_ID    = 'lastImportId';
    private const REDIS_MIN_CLICKHOUSE_ID = 'minClickHouseId';
    private const REDIS_IMPORT_ROWS       = 'countImportRows';

    /**
     * Cli param --exportAgain=1|0
     * will truncate all events from ClickHouse and reimport all data from BigQuery
     *
     * @var bool
     */
    public bool $exportAgain = false;

    protected array $columnList = [
        'id', 'offerType', 'date', 'createdAt', 'offerId', 'affiliateId', 'advertiserId',
        'agentId', 'typeId', 'typeSubId', 'isUnique', 'ip', 'transactionId', 'country',
        'stateId', 'verticalId', 'payoutTypeId', 'conversionsTrackTypeId', 'adCampaignId',
        'channelId', 'categoryId', 'goalId', 'creativeId', 'browserId', 'platformId', 'affSource',
        'affSub1', 'affSub2', 'affSub3', 'affSub4', 'affSub5', 'sale', 'cost', 'revenue', 'payoutAffiliate',
        'payoutAgent', 'url', 'referer'
    ];

    /** @var BigQuery|null $dbBigQuery */
    private ?BigQuery $dbBigQuery;

    /** @var ClickHouseConnection|null $ch */
    private ?ClickHouseConnection $ch;

    /** @var RedisStorage|null $ch */
    private ?RedisStorage $globalStorage;

    private string $tableNameBigQuery   = 'track_api_events';
    private string $tableNameClickHouse = 'lynx_api_events';

    private string $redisKey = 'bigquery_to_clickhouse';

    private string $date = '';

    private string $startDate;

    private array $insertRows = [];

    private int $limit = 5000;

    private int $lastId = 0;

    private int $maxId = 0;

    private int $totalCounter = 0;

    private int $totalDayCounter = 0;

    private int $dayCounter = 0;

    public function init()
    {
        parent::init();

        $this->dbBigQuery = Yii::$app->dbBigQuery;
        $this->ch = Yii::$app->get('clickhouse');
        $this->ch->open();
        $this->globalStorage = \Yii::$app->memoryStorage->redis;
        $this->globalStorage->setDb(\Yii::$app->params['storage']['memory']['redis']['db']['globalStorage']);
    }

    public function options($actionID)
    {
        return ['exportAgain'];
    }

    public function actionIndex()
    {
        if (!$this->ch->getClient()->ping()) {
            $this->echo("\nERROR! ClickHouse connection/ping failed\n");
            return;
        }

        if (!$this->dbBigQuery->isClientOk()) {
            $this->echo("\nERROR! BigQuery connection failed\n");
            return;
        }

        if ($this->exportAgain) {
            $this->clearOldData();
        }

        $lastImport = $this->getFromRedis();

        if (!empty($lastImport)) {
            $this->echoWithSeparator('Current State:');
            $this->echoWithSeparator('  Date: ' . $lastImport[self::REDIS_LAST_IMPORT_DATE]);
            $this->echoWithSeparator('  LastId: ' . $lastImport[self::REDIS_LAST_IMPORT_ID]);
            $this->echoWithSeparator('  Imported Rows: ' . $lastImport[self::REDIS_IMPORT_ROWS]);

            $this->setDate($lastImport[self::REDIS_LAST_IMPORT_DATE]);
            $this->startDate = $lastImport[self::REDIS_LAST_IMPORT_DATE];
            $this->lastId = $lastImport[self::REDIS_LAST_IMPORT_ID];
            $this->maxId = $lastImport[self::REDIS_MIN_CLICKHOUSE_ID];
            $this->totalCounter = $lastImport[self::REDIS_IMPORT_ROWS];
        }

        $timeStart = time();

        if (0 == $this->maxId) {
            /** @var Statement $statement */
            $statement = $this->ch->getClient()->select("SELECT MIN(id) as id FROM " . $this->tableNameClickHouse . " LIMIT 1");
            $row = $statement->fetchOne();

            if (isset($row['id']) && !empty($row['id'])) {
                $this->maxId = $row['id'];
            }
        }

        if (empty($this->getDate())) {
            $trackActEventsSql = "
                select MIN(createdAt) as date from track_act_events;
            ";
            $trackActEvents = Yii::$app->dbTrack->createCommand($trackActEventsSql)->queryOne();
            $this->startDate = date('Y-m-d', strtotime($trackActEvents['date']));

            $this->setDate($this->startDate);
        }

        while ($this->processDataTransfer()) {
            // loop until processDataTransfer() return true
            gc_collect_cycles();
            // echo "     Memory: " . memory_get_usage() . "\n";
        }

        $durationSeconds = time() - $timeStart;

        $hours = floor($durationSeconds / 3600);
        $mins = floor($durationSeconds / 60 % 60);
        $secs = floor($durationSeconds % 60);

        $durationTime = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);

        $this->echoWithSeparator('Total count rows: ' . $this->totalCounter);
        $this->echoWithSeparator('Time: ' . $durationTime);
    }

    private function setDate(string $date): void
    {
        $this->date = $date;
    }

    private function getDate(): string
    {
        return $this->date;
    }

    private function processDataTransfer(): bool
    {
        if (strtotime($this->getDate()) == strtotime($this->getNextDay(date('Y-m-d')))) {
            return false;
        }

        $bigQueryRows = $this->getBigQueryRows();

        foreach ($bigQueryRows as $row) {
            $this->insertRows[] = $this->getValues($row);
            $this->dayCounter++;
            $this->lastId = (int)$row['id'];
        }

        unset($row);

        if ($this->limit == $this->dayCounter) {
            $this->insertRows = array_filter($this->insertRows, fn($row) => $this->lastId != (int)$row['id']);
            $this->lastId = (int)$this->insertRows[array_key_last($this->insertRows)]['id'];
        }

        if (!empty($this->insertRows)) {
            $this->ch->getClient()->insert(
                $this->tableNameClickHouse,
                $this->insertRows,
                $this->columnList
            );
            $this->totalCounter += count($this->insertRows);
            $this->totalDayCounter += count($this->insertRows);
        }

        $this->clearInsertRows();
        $separator = "\r";
        $currentDate = $this->getDate();
        $fullDayCounter = $this->totalDayCounter;

        if ($this->limit > $this->dayCounter) {
            $nextDate = $this->getNextDay($this->getDate());
            $this->lastId = 0;
            $currentDate = $this->getDate();
            $this->setDate($nextDate);
            $this->totalDayCounter = 0;
            $separator = "\n";
        }

        $this->setToRedis($this->startDate, $currentDate, $this->lastId, $this->maxId, $this->totalCounter);

        $this->echoWithSeparator(
            'Day ' . $currentDate . ' count rows: ' . $fullDayCounter . ', Last Id: ' . $this->lastId,
            $separator
        );

        $this->dayCounter = 0;

        return true;
    }

    protected function getBigQueryRows()
    {
        $maxIdWhere = $this->maxId > 0 ? "and id < " . $this->maxId : "";
        $sql = "
            select * from " . $this->dbBigQuery->dataset . "." . $this->tableNameBigQuery . "
            where date >= '" . $this->getDate() . "' and date <= '" . $this->getDate() . "' 
                and id > " . $this->lastId . " " . $maxIdWhere . "
            order by id limit " . $this->limit . ";
        ";

        $result = $this->dbBigQuery->runQuery($sql);

        return $result->rows();
    }

    private function getValues(array &$row): array
    {
        /** @var Date $data */
        $data = $row['date'];
        $row['date'] = substr($data->formatAsString(), 0, 10);

        /** @var DateTime $createdAt */
        $createdAt = $row['createdAt'];
        $row['createdAt'] = date('Y-m-d H:i:s', $createdAt->getTimestamp());

        $row = array_map(function ($value) {
            if (empty($value) && is_null($value)) {
                $value = '';
            }
            return $value;
        }, $row);

        $row['country'] = empty($row['country']) ? '' : $row['country'];

        return $row;
    }

    private function getNextDay(string $date): string
    {
        return date('Y-m-d', strtotime('+1 day', strtotime($date)));
    }

    private function clearInsertRows(): void
    {
        $this->insertRows = [];
    }

    protected function echoWithSeparator(string $str, string $separator = "\n"): void
    {
        echo $str . $separator;
    }

    private function setToRedis(
        string $startDate,
        string $lastImportDate,
        int $lastImportId,
        int $minClickHouseId,
        int $countImportRows
    ): void {
        $value = serialize([
            self::REDIS_START_DATE        => $startDate,
            self::REDIS_LAST_IMPORT_DATE  => $lastImportDate,
            self::REDIS_LAST_IMPORT_ID    => $lastImportId,
            self::REDIS_MIN_CLICKHOUSE_ID => $minClickHouseId,
            self::REDIS_IMPORT_ROWS       => $countImportRows,
        ]);

        $this->globalStorage->set($this->getRedisKey(), $value, 3600 * 24 * 30);
    }

    private function getFromRedis(): array
    {
        $value = $this->globalStorage->getByKey($this->getRedisKey());

        if (!$value) {
            $value = [];
        } else {
            $value = unserialize($value);
        }

        return $value;
    }

    protected function getRedisKey(): string
    {
        return $this->redisKey;
    }

    private function clearOldData(): void
    {
        $this->globalStorage->del($this->getRedisKey());

        $sql = "TRUNCATE TABLE IF EXISTS " . $this->tableNameClickHouse;

        if ($cluster = $this->ch->getCluster()) {
            $sql .= ' ON CLUSTER ' . $cluster;
        }

        $this->ch->getClient()->write($sql);
    }
}
