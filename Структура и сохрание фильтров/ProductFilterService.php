<?php

namespace frontend\services\ProductFilter;

use api\helpers\ArrayHelper;
use api\models\Product;
use Yii;
use yii\redis\Connection;

/**
 * Class ProductFilterService
 * @package frontend\services\ProductFilter
 */
class ProductFilterService
{
    public const FILTER_ATTR   = 'attributes';
    public const FILTER_PRICE  = 'price';
    public const FILTER_CHOSEN = 'chosen';

    public const TYPE_ATTR  = 'attribute';
    public const TYPE_BRAND = 'brand';
    public const TYPE_PRICE = 'price';

    private const PREFIX_FILTER = 'filters:';

    private $paramPriceFrom = 0.0;
    private $paramPriceTo = 0.0;
    private $paramBrands = [];
    private $paramAttributeValues = [];

    private $queryBrands     = [];
    private $queryAttributes = [];

    private $filterAttributes = [];
    private $filterBrands     = [];
    private $filterPrices     = [];

    private $chosenFilters = [];

    /**
     * @var Connection
     */
    private $storage;

    /**
     * @return float
     */
    public function getParamPriceFrom(): ?float
    {
        return $this->paramPriceFrom ?? null;
    }

    /**
     * @param float $paramPriceFrom
     *
     * @return $this
     */
    public function setParamPriceFrom(float $paramPriceFrom): self
    {
        $this->paramPriceFrom = $paramPriceFrom;

        return $this;
    }

    /**
     * @return float
     */
    public function getParamPriceTo(): ?float
    {
        return $this->paramPriceTo ?? null;
    }

    /**
     * @param float $paramPriceTo
     *
     * @return $this
     */
    public function setParamPriceTo(float $paramPriceTo): self
    {
        $this->paramPriceTo = $paramPriceTo;

        return $this;
    }

    /**
     * @return array
     */
    public function getParamBrands(): array
    {
        return $this->paramBrands;
    }

    /**
     * @param array $paramBrands
     *
     * @return $this
     */
    public function setParamBrands(array $paramBrands): self
    {
        $this->paramBrands = $paramBrands;

        return $this;
    }

    /**
     * @return array
     */
    public function getParamAttributeValues(): array
    {
        return $this->paramAttributeValues;
    }

    /**
     * @param array $paramAttributeValues
     *
     * @return $this
     */
    public function setParamAttributeValues(array $paramAttributeValues): self
    {
        $this->paramAttributeValues = $paramAttributeValues;

        return $this;
    }

    /**
     * @return array
     */
    public function getQueryBrands(): array
    {
        return $this->queryBrands;
    }

    /**
     * @param array $queryBrands
     *
     * @return $this
     */
    public function setQueryBrands(array $queryBrands): self
    {
        $this->queryBrands = $queryBrands;

        return $this;
    }

    /**
     * @return array
     */
    public function getQueryAttributes(): array
    {
        return $this->queryAttributes;
    }

    /**
     * @param array $queryAttributes
     *
     * @return $this
     */
    public function setQueryAttributes(array $queryAttributes): self
    {
        $this->queryAttributes = $queryAttributes;

        return $this;
    }

    /**
     * ProductFilterService constructor.
     *
     * @param Connection $storage
     */
    public function __construct(Connection $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param Product $product
     * @param float   $price
     *
     * @return $this
     */
    public function setProductFilters(Product $product, float $price): self
    {
        $this->prepareFiltersStructure($product, $price);

        return $this;
    }

    /**
     * @param string $categorySlug
     *
     * @return array
     */
    public function getCategoryFilters(string $categorySlug): array
    {
        return $this->prepareFilters($categorySlug);
    }

    /**
     * @param string $categorySlug
     *
     * @return array
     */
    private function prepareFilters(string $categorySlug): array
    {
        $categoryFilters = $this->getFromStorage($categorySlug);

        foreach ($categoryFilters[self::FILTER_ATTR] as &$filter) {
            if (self::TYPE_BRAND === $filter['type']) {
                $this->checkValue($filter['type'], $filter['values'], $this->getParamBrands(), $this->getQueryBrands());
            }

            if (self::TYPE_ATTR === $filter['type']) {
                $this->checkValue($filter['type'], $filter['values'], $this->getParamAttributeValues(), $this->getQueryAttributes());
            }
        }

        if (!empty($this->getParamPriceFrom()) && !empty($this->getParamPriceTo())) {
            $categoryFilters[self::FILTER_PRICE]['chosenValues'] = [
                'min' => $this->getParamPriceFrom(),
                'max' => $this->getParamPriceTo()
            ];
            $this->chosenFilters[] = [
                'type' => self::FILTER_PRICE,
                'name' => $this->getParamPriceFrom() . '-' . $this->getParamPriceTo()
            ];
        }

        $categoryFilters[self::FILTER_CHOSEN] = $this->chosenFilters;

        return $categoryFilters;
    }

    /**
     * @param Product $product
     * @param float   $price
     *
     * @throws \Exception
     */
    private function prepareFiltersStructure(Product $product, float $price): void
    {
        $price = (int)$price;

        $categories = $product->category->parents()
            ->select('slug')
            ->asArray()
            ->all();

        $categories[] = ['slug' => $product->category->slug];

        foreach ($categories as $category) {
            if (!isset($this->filterPrices[$category['slug']][self::FILTER_PRICE])) {
                $this->filterPrices[$category['slug']][self::FILTER_PRICE] = [
                    'type'          => self::TYPE_PRICE,
                    'attributeName' => Yii::t('filters', 'price'),
                    'rangeValues'   => [
                        'min' => $price,
                        'max' => $price
                    ]
                ];
            }

            $minPrice = $this->filterPrices[$category['slug']][self::FILTER_PRICE]['rangeValues']['min'];
            $maxPrice = $this->filterPrices[$category['slug']][self::FILTER_PRICE]['rangeValues']['max'];

            $this->filterPrices[$category['slug']][self::FILTER_PRICE]['rangeValues'] = [
                'min' => $price < $minPrice ? $price : $minPrice,
                'max' => $price > $maxPrice ? $price : $maxPrice,
            ];

            if (!isset($this->filterBrands[$category['slug']][self::FILTER_ATTR][0])) {
                $this->filterBrands[$category['slug']][self::FILTER_ATTR][0] = [
                    'type'          => self::TYPE_BRAND,
                    'attributeName' => Yii::t('filters', 'brand'),
                    'values'        => []
                ];
            }

            if (0 != $product->brand_id) {
                $this->filterBrands[$category['slug']][self::FILTER_ATTR][0]['values'][$product->brand_id] = [
                    'valueId'      => $product->brand_id,
                    'valueName'    => $product->productBrand->name,
                    'checked'      => 0,
                    'disabled'     => 0,
                    'countProduct' => 0
                ];
            }
        }

        foreach ($product->attributeValue as $attributeValue) {
            $attributeId   = (int)$attributeValue->mainAttribute->id;
            $valueId       = (int)$attributeValue->id;
            $attributeName = (string)ArrayHelper::getValue($attributeValue, 'mainAttribute.translate.0.name', '');
            $valueName     = (string)ArrayHelper::getValue($attributeValue, 'translate.0.name', '');

            foreach ($categories as $category) {
                if (!isset($this->filterAttributes[$category['slug']][self::FILTER_ATTR][$attributeId])) {
                    $this->filterAttributes[$category['slug']][self::FILTER_ATTR][$attributeId] = [
                        'type'          => self::TYPE_ATTR,
                        'attributeName' => $attributeName,
                        'values'        => []
                    ];
                }

                $this->filterAttributes[$category['slug']][self::FILTER_ATTR][$attributeId]['values'][$valueId] = [
                    'valueId'      => $valueId,
                    'valueName'    => $valueName,
                    'checked'      => 0,
                    'disabled'     => 0,
                    'countProduct' => 0
                ];
            }
        }
    }

    /**
     * @{inheritDoc}
     */
    public function saveToStorage(): void
    {

        foreach ($this->filterBrands as &$brand) {
            $this->unsetArrayIndexes($brand[self::FILTER_ATTR]);
        }

        foreach ($this->filterAttributes as $categorySlug => &$attribute) {
            $this->unsetArrayIndexes($attribute[self::FILTER_ATTR]);

            $this->filterAttributes[$categorySlug][self::FILTER_ATTR] = array_merge(
                $attribute[self::FILTER_ATTR],
                $this->filterBrands[$categorySlug][self::FILTER_ATTR]
            );

            $this->filterAttributes[$categorySlug] = array_merge(
                $attribute,
                $this->filterPrices[$categorySlug]
            );

            if (!empty($this->filterAttributes[$categorySlug])) {
                $this->setToStorage($categorySlug, $this->filterAttributes[$categorySlug]);
            }
        }
    }

    /**
     * @param string $type
     * @param array  $values
     * @param array  $filterValues
     * @param array  $queryValues
     */
    private function checkValue(string $type, array &$values, array $filterValues, array $queryValues): void
    {
        foreach ($values as &$value) {
            if ($this->isExists($value['valueId'], $filterValues)) {
                $value['checked'] = 1;
                $this->chosenFilters[] = [
                    'type' => $type,
                    'id'   => $value['valueId'],
                    'name' => $value['valueName'],
                ];
            }
            if (!$this->isExists($value['valueId'], $queryValues)) {
                $value['disabled'] = 1;
            }
        }
    }

    /**
     * @param int   $valueId
     * @param array $values
     *
     * @return bool
     */
    private function isExists(int $valueId, array $values): bool
    {
        return array_key_exists($valueId, array_flip($values));
    }

    /**
     * @param array $array
     */
    private function unsetArrayIndexes(array &$array): void
    {
        $array = array_values($array);
        foreach ($array as $index => $value) {
            if (isset($value['values']) && is_array($value['values'])) {
                $array[$index]['values'] = array_values($value['values']);
            }
        }
    }

    /**
     * @param string $categorySlag
     * @param array  $filters
     */
    private function setToStorage(string $categorySlag, array $filters): void
    {
        $this->storage->set($this->storageKey($categorySlag), serialize($filters));
    }

    /**
     * @param string $categorySlag
     *
     * @return array
     */
    public function getFromStorage(string $categorySlag): array
    {
        return unserialize($this->storage->get($this->storageKey($categorySlag))) ?? [];
    }

    /**
     * @param string $categorySlag
     *
     * @return string
     */
    private function storageKey(string $categorySlag): string
    {
        return self::PREFIX_FILTER . $categorySlag;
    }
}
